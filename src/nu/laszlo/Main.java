package nu.laszlo;
import java.util.stream.IntStream;
import org.apache.commons.lang3.time.StopWatch;

public class Main {
    static boolean isPrime(int i) {
        int limit = (int)Math.sqrt(i)+1;
        return !IntStream.range(2, limit)
                .anyMatch(n -> i % n == 0);
    }

    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch();
        int n = 10001;
        stopWatch.start();
        int nthPrime = IntStream.iterate(2, i -> i+1)
                .filter(Main::isPrime)
                .skip(n-1)
                .findFirst().getAsInt();
        stopWatch.stop();
        System.out.printf("The %dth prime is %d\n", n, nthPrime);
        System.out.printf("Execution time: %.2fs\n", (float)stopWatch.getTime()/1000);
    }
}